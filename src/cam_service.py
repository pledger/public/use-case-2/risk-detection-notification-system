from ldm import LDM
from own_prometheus_client import PrometheusClient
from instances import Instances, Sockets
from utils import timestamp_to_delta_time
import v2xlib.v2xcom as v2xcom
from v2xlib.messages.cam import CAM, GenerationDeltaTime, CoopAwareness
from v2xlib.messages.its_container import StationID
from v2xlib.utils.encoder import Encoder
import time


class CAMService:
    def __init__(self, period: int, station_id: int, broker_host: str, broker_port: int, instances: Instances, ldm: LDM, prometheus_client: PrometheusClient):
        super().__init__()
        self.ldm = ldm
        self.prometheus_client = prometheus_client
        # self.connection = v2xcom.Connection(broker_host=broker_host, broker_port=broker_port, connection_name="camservice")
        # self.connection.connect()
        # self.instance = v2xcom.Instance(instance_name, self.connection)
        self.instances = instances
        self.encoder = Encoder()
        self.socket_config = v2xcom.config.SocketConfig()
        self.socket_config.set_btp_port(2001)
        self.socket_config.set_general_name("CAM")
        self.socket_config.set_general_type("cam")
        self.socket_config.set_general_timeout(600000000)
        self.socket = self.instances.open_socket(
            self.socket_config, self.on_receive)
        self.socket.set_payload_cb(self.on_receive_payload)
        self.cam = CAM.from_xml("""<CAM>
    <header>
        <protocolVersion>2</protocolVersion>
        <messageID>2</messageID>
        <stationID>1</stationID>
    </header>
    <cam>
        <generationDeltaTime>1</generationDeltaTime>
        <camParameters>
            <basicContainer>
                <stationType>11</stationType>
                <referencePosition>
                    <latitude>900000001</latitude>
                    <longitude>1800000001</longitude>
                    <positionConfidenceEllipse>
                        <semiMajorConfidence>4095</semiMajorConfidence>
                        <semiMinorConfidence>4095</semiMinorConfidence>
                        <semiMajorOrientation>3601</semiMajorOrientation>
                    </positionConfidenceEllipse>
                    <altitude>
                        <altitudeValue>16380</altitudeValue>
                        <altitudeConfidence><alt-050-00/></altitudeConfidence>
                    </altitude>
                </referencePosition>
            </basicContainer>
            <highFrequencyContainer>
                <basicVehicleContainerHighFrequency>
                    <heading>
                        <headingValue>0</headingValue>
                        <headingConfidence>10</headingConfidence>
                    </heading>
                    <speed>
                        <speedValue>0</speedValue>
                        <speedConfidence>1</speedConfidence>
                    </speed>
                    <driveDirection><forward/></driveDirection>
                    <vehicleLength>
                        <vehicleLengthValue>1023</vehicleLengthValue>
                        <vehicleLengthConfidenceIndication><noTrailerPresent/></vehicleLengthConfidenceIndication>
                    </vehicleLength>
                    <vehicleWidth>20</vehicleWidth>
                    <longitudinalAcceleration>
                        <longitudinalAccelerationValue>161</longitudinalAccelerationValue>
                        <longitudinalAccelerationConfidence>0</longitudinalAccelerationConfidence>
                    </longitudinalAcceleration>
                    <curvature>
                        <curvatureValue>0</curvatureValue>
                        <curvatureConfidence><unavailable/></curvatureConfidence>
                    </curvature>
                    <curvatureCalculationMode><yawRateUsed/></curvatureCalculationMode>
                    <yawRate>
                        <yawRateValue>4199</yawRateValue>
                        <yawRateConfidence><degSec-000-01/></yawRateConfidence>
                    </yawRate>
                </basicVehicleContainerHighFrequency>
            </highFrequencyContainer>
        </camParameters>
    </cam>
</CAM>""")
        self.cam.header.stationID = StationID(station_id)
        self.period = period

    def on_receive(self, message):
        pass

    def on_receive_payload(self, topic:str, message:bytes):
        received_cam = self.encoder.decode_cam(message)
        self.ldm.new_position(received_cam.header.stationID.__dict__(), received_cam.cam)
        self.prometheus_client.new_cam(received_cam)

    def send_cam(self, coop: CoopAwareness):
        self.cam.cam = coop
        self.socket.send(self.encoder.encode_cam(self.cam))

    def start(self):
        while True:
            station_id = self.ldm.get_position(
                self.cam.header.stationID.__dict__())
            if station_id:
                self.send_cam(station_id)
            else:
                print("No Tram CAM sent because there is no position available!")
            time.sleep(self.period/1000)
