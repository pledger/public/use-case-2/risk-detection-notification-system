from ldm import LDM
from v2xlib.messages.cam import *
from dateutil import parser
import socket
import json
import sys
import time

class BSC:
    def __init__(self, ldm: LDM, station_id: int, gps_host: str = 'localhost', gps_port: int = 2947):
        super().__init__()
        self.ldm = ldm
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(10)
        self.gps_host = gps_host
        self.gps_port = gps_port
        self.station_id = station_id

    def start(self):
        connected = False
        while not connected:
            try:
                self.socket.connect((self.gps_host, self.gps_port))
                connected = True
            except socket.error as msg:
                print ("Socket Error: %s" % msg)
                time.sleep(10)
        self.socket.send('?WATCH={"enable":true,"json":true};'.encode('utf-8'))
        self.socket.recv(2048)
        self.socket.recv(2048)
        try:
            while True:
                current_message = self.socket.recv(2048)
                if self.line_to_be_saved(current_message):
                    temp_coop = CoopAwareness.from_xml("""<CoopAwareness>
        <generationDeltaTime>1</generationDeltaTime>
        <camParameters>
            <basicContainer>
                <stationType>11</stationType>
                <referencePosition>
                    <latitude>900000001</latitude>
                    <longitude>1800000001</longitude>
                    <positionConfidenceEllipse>
                        <semiMajorConfidence>4095</semiMajorConfidence>
                        <semiMinorConfidence>4095</semiMinorConfidence>
                        <semiMajorOrientation>3601</semiMajorOrientation>
                    </positionConfidenceEllipse>
                    <altitude>
                        <altitudeValue>16380</altitudeValue>
                        <altitudeConfidence><alt-050-00/></altitudeConfidence>
                    </altitude>
                </referencePosition>
            </basicContainer>
            <highFrequencyContainer>
                <basicVehicleContainerHighFrequency>
                    <heading>
                        <headingValue>0</headingValue>
                        <headingConfidence>10</headingConfidence>
                    </heading>
                    <speed>
                        <speedValue>0</speedValue>
                        <speedConfidence>1</speedConfidence>
                    </speed>
                    <driveDirection><forward/></driveDirection>
                    <vehicleLength>
                        <vehicleLengthValue>1023</vehicleLengthValue>
                        <vehicleLengthConfidenceIndication><noTrailerPresent/></vehicleLengthConfidenceIndication>
                    </vehicleLength>
                    <vehicleWidth>20</vehicleWidth>
                    <longitudinalAcceleration>
                        <longitudinalAccelerationValue>161</longitudinalAccelerationValue>
                        <longitudinalAccelerationConfidence>0</longitudinalAccelerationConfidence>
                    </longitudinalAcceleration>
                    <curvature>
                        <curvatureValue>0</curvatureValue>
                        <curvatureConfidence><unavailable/></curvatureConfidence>
                    </curvature>
                    <curvatureCalculationMode><yawRateUsed/></curvatureCalculationMode>
                    <yawRate>
                        <yawRateValue>4199</yawRateValue>
                        <yawRateConfidence><degSec-000-01/></yawRateConfidence>
                    </yawRate>
                </basicVehicleContainerHighFrequency>
            </highFrequencyContainer>
        </camParameters>
    </CoopAwareness>""")
                    gps_parameters = json.loads(
                        current_message.decode('utf-8'))
                    self.set_gps_parameters(temp_coop, gps_parameters)
                    self.ldm.new_position(self.station_id, temp_coop)
        finally:
            self.socket.close()

    def line_to_be_saved(self, current_line):
        try:
            parsed_line = json.loads(current_line.decode('utf-8'))
            if 'class' in parsed_line:
                if parsed_line['class'] == 'DEVICES' or parsed_line['class'] == 'WATCH' or parsed_line['class'] == 'DEVICE':
                    return True
                # if parsed_line['class'] == 'TPV' and self.has_all_the_parameters(parsed_line):
                if parsed_line['class'] == 'TPV':
                    return True
                if parsed_line['class'] == 'SKY':
                    return False
                print('Info received from GPS not completed')
                return False

        except:
            print("Information given by the GPSD not recognized")
            return False

    def has_all_the_parameters(self, tpv_line):
        if('device' in tpv_line and
            'mode' in tpv_line and
            'time' in tpv_line and
            'ept' in tpv_line and
            'lat' in tpv_line and
            'lon' in tpv_line and
            'alt' in tpv_line and
            'epv' in tpv_line and
            'track' in tpv_line and
            'speed' in tpv_line and
            'climb' in tpv_line and
            'epx' in tpv_line and
                'epy' in tpv_line):
            return True
        return False

    def set_gps_parameters(self, coop: CoopAwareness, gps_parameters):
        if "time" in gps_parameters:
            time = int(((parser.parse(gps_parameters["time"]).timestamp()) - 1072911600) % 65536)
            coop.generationDeltaTime = GenerationDeltaTime(
                time)
        if "lat" in gps_parameters:
            coop.camParameters.basicContainer.referencePosition.latitude = Latitude(
                int(gps_parameters["lat"]* 10 ** 7))
        if "lon" in gps_parameters:
            coop.camParameters.basicContainer.referencePosition.longitude = Longitude(
                int(gps_parameters["lon"]* 10 ** 7))
        if "epx" and "epy" in gps_parameters:
            coop.camParameters.basicContainer.referencePosition.positionConfidenceEllipse = self.create_position_confidence(gps_parameters["epx"],
                                                                                                                            gps_parameters["epy"])
        if "alt" in gps_parameters:
            coop.camParameters.basicContainer.referencePosition.altitude = self.create_altitude(
                gps_parameters["alt"])
        if "epv" in gps_parameters:
            coop.camParameters.basicContainer.referencePosition.altitude.altitudeConfidence = self.create_altitude_confidence(
                gps_parameters["epv"])
        if "track" in gps_parameters:
            coop.camParameters.highFrequencyContainer.value.heading = self.create_heading(
                gps_parameters["track"])
        if "epd" in gps_parameters:
            coop.camParameters.highFrequencyContainer.value.heading.headingConfidence = self.create_heading_confidence(
                gps_parameters["epd"])
        if "speed" in gps_parameters:
            coop.camParameters.highFrequencyContainer.value.speed = self.create_speed(
                gps_parameters["speed"])
        if "eps" in gps_parameters:
            coop.camParameters.highFrequencyContainer.value.speed.speedConfidence = self.create_speed_confidence(
                gps_parameters["eps"])

    def create_position_confidence(self, epx, epy) -> PosConfidenceEllipse:
        positionConfidenceEllipse = PosConfidenceEllipse(
            SemiAxisLength(int(epx * 100)),
            SemiAxisLength(int(epy * 100)),
            HeadingValue(0)
        )
        if epy >= epx:
            positionConfidenceEllipse = PosConfidenceEllipse(
                SemiAxisLength(int(epy * 100)),
                SemiAxisLength(int(epx * 100)),
                HeadingValue(0)
            )
        return positionConfidenceEllipse

    def create_altitude(self, altitude: float) -> Altitude:
        altitude_return = Altitude(
            AltitudeValue(int(altitude * 100)),
            AltitudeConfidence.unavailable
        )
        if altitude > 8000:
            altitude_return.altitudeValue = AltitudeValue(800000)
        elif altitude < -1000:
            altitude_return.altitudeValue = AltitudeValue(-100000)
        return altitude_return

    def create_altitude_confidence(self, epv) -> AltitudeConfidence:
        altitudeConfidence = AltitudeConfidence.unavailable
        if epv < 0.01:
            altitudeConfidence = AltitudeConfidence.alt_000_01
        elif epv < 0.02:
            altitudeConfidence = AltitudeConfidence.alt_000_02
        elif epv < 0.05:
            altitudeConfidence = AltitudeConfidence.alt_000_05
        elif epv < 0.1:
            altitudeConfidence = AltitudeConfidence.alt_000_10
        elif epv < 0.2:
            altitudeConfidence = AltitudeConfidence.alt_000_20
        elif epv < 0.5:
            altitudeConfidence = AltitudeConfidence.alt_000_50
        elif epv < 1:
            altitudeConfidence = AltitudeConfidence.alt_001_00
        elif epv < 2:
            altitudeConfidence = AltitudeConfidence.alt_002_00
        elif epv < 5:
            altitudeConfidence = AltitudeConfidence.alt_005_00
        elif epv < 10:
            altitudeConfidence = AltitudeConfidence.alt_010_00
        elif epv < 20:
            altitudeConfidence = AltitudeConfidence.alt_020_00
        elif epv < 50:
            altitudeConfidence = AltitudeConfidence.alt_050_00
        elif epv < 100:
            altitudeConfidence = AltitudeConfidence.alt_100_00
        elif epv <= 200:
            altitudeConfidence = AltitudeConfidence.alt_200_00
        elif epv > 200:
            altitudeConfidence = AltitudeConfidence.outOfRange
        return altitudeConfidence

    def create_heading(self, track) -> Heading:
        return Heading(
            HeadingValue(int(track * 10)),
            HeadingConfidence(126)
        )

    def create_heading_confidence(self, epd) -> HeadingConfidence:
        headingConfidence = HeadingConfidence(126)
        if epd <= 12.5:
            headingConfidence = HeadingConfidence(int(epd * 10))

    def create_speed(self, speed) -> Speed:
        speed_return = Speed(
            SpeedValue(int(speed * 100)),
            SpeedConfidence(127)
        )
        if speed > 163.82:
            speed_return.speedValue = SpeedValue(16382)
        return speed_return

    def create_speed_confidence(self, eps) -> SpeedConfidence:
        speedConfidence = SpeedConfidence(127)
        if eps > 1.25:
            speedConfidence = SpeedConfidence(126)
        else:
            speedConfidence = SpeedConfidence(int(eps * 100))
        return speedConfidence
