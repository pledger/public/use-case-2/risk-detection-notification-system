# Risk Detection Notification System

## Introduction

The **Risk Detection Notification System** is the module that aggregates all of the information gathered by the RSUs and Cameras to notify and alert possible collision risks to all of the entities involved within the mobility environtment.

To do so, acts on the "Facilities layer" of the ETSI's C-ITS protocol stack by having a Local Dynamic Map (LDM) that tracks the revealed positions of all the moving parts of the mobility environment through all the different sensors. Especially the CAM messages sent through V2X communications.

The module is designed to run on an edge server; from which it has a low latency to operate with the vehicular communicatuons. That's the reason why the notifications and alerts are sent through VANETs; using the specified messages in the ETSI C-ITS stack.

## Architecture

![PLEDGER Notification system architecure](docs/pledger_architecture.png)

The architecture of the module regards on the fact that connects with the **V2XCOM** (The I2CAT module based on vanetza that implements part of the ETSI protocol stack) through MQTT queues. Which means that everytime the module is deployed; needs to be along an **MQTT broker** and one or more **V2XCOM** running on theiur respective **RSU**s.

On the other hand, it gathers positional information coming from other sensors (the BSC module in that case) through TCP sockets that sent the positional information the same way a **GPSD** would do. With **TPV** data objects like the one shown below:
```
{"class":"TPV","device":"/dev/pts/1",
    "time":"2005-06-08T10:34:48.283Z","ept":0.005,
    "lat":46.498293369,"lon":7.567411672,"alt":1343.127,
    "eph":36.000,"epv":32.321,
    "track":10.3788,"speed":0.091,"climb":-0.085,"mode":3}
``` 
For more information regarding the GPSD information, there is enoguh with visiting: [GPSD Information](https://gpsd.gitlab.io/gpsd/gpsd_json.html)

## Configuring

When launching the module, it's important to properly configure the launching arguments so the module manages to interconnect to the right nodes.

The arguments to be configured are the following ones:

- **period**: Time between CAM messages emitted (in Milliseconds) [By default: 1000]
- **station_id**: Station id in CAM messages sent as a proxy to communicate the positions gathered by the BSC module. [By default: 1]
- **mqtt_host**: The broker address [By default: localhost]
- **mqtt_port**: The broker port [By default: 1883]
- **gps_host**: The address to the BSC module [By default: localhost]
- **gps_port**: The port to the BSC module [By default: 2947]
- **instances**: Multiple value argument. Every V2XCOM-RSU has an instance name. In order to publish the sent messages to this modules, they have to be specified through this arg.


## Building and Running

The current module has been designed to be run as a docker container. To build the container there is enough with building the docker image specified in the *Dockerfile* file:
```
docker build -t notification .
```
Once the docker image has been built, the module can be launched with simply running the container (with the appropriate configuration)
```
docker run notification --instances test1  
```

It's important to notice that the module is thought to be run within a *docker-compose* or *kubernetes* cluster. So it is aligned with all of the needed modules.

| ![EU Flag](http://www.consilium.europa.eu/images/img_flag-eu.gif) | This work has received funding by the European Commission under grant agreement No. 871536, Pledger project. |
|---|--------------------------------------------------------------------------------------------------------|